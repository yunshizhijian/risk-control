import {notification} from 'ant-design-vue'

const respCommon = {
  /**
   * 响应数据之前做点什么
   * @param response 响应对象
   * @param options 应用配置 包含: {router, i18n, store, message}
   * @returns {*}
   */
  onFulfilled(response, options) {
    let {code, data} = response.data
    let {router,store} = options
    store.state.loading.isLoading = false
    switch (code) {
      case 500:
        notification.error({
          message: '网络或系统错误，请联系管理员。',
          description: `原因：${data.cause}\n类型：${data.type}\n行号：第${data.lineNumber}行\n文件名：${data.fileName}\n方法名：${data.methodName}`,
          duration: 0,
          style: {whiteSpace: 'pre-line'}
        })
        break
      case 2001:
        router.push({path: '/login'})
        notification.error({
          message: '未登录或登录已过期!'
        })
        break
    }
    return response
  },
  /**
   * 响应出错时执行
   * @param error 错误对象
   * @param options 应用配置 包含: {router, i18n, store, message}
   * @returns {Promise<never>}
   */
  onRejected(error, options) {
    notification.error({
      message: '网络或系统错误，请联系管理员。',
      description: error.message
    })
    return Promise.reject(error)
  }
}

const reqCommon = {
  /**
   * 发送请求之前做些什么
   * @param config axios config
   * @param options 应用配置 包含: {router, i18n, store, message}
   * @returns {*}
   */
  onFulfilled(config, options) {
    const {store} = options
    store.state.loading.isLoading = true
    // const {message} = options
    // const {url, xsrfCookieName} = config
    // if (url.indexOf('login') === -1 && xsrfCookieName && !Cookie.get(xsrfCookieName)) {
    //   message.warning('认证 token 已过期，请重新登录')
    // }
    return config
  },
  /**
   * 请求出错时做点什么
   * @param error 错误对象
   * @param options 应用配置 包含: {router, i18n, store, message}
   * @returns {Promise<never>}
   */
  onRejected(error, options) {
    const {message} = options
    message.error(error.message)
    return Promise.reject(error)
  }
}

export default {
  request: [reqCommon], // 请求拦截
  response: [respCommon] // 响应拦截
}
