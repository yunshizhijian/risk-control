import {BASE_URL} from '@/services/api'
import {METHOD, request} from '@/utils/request'

export async function queryModelList(payload) {
  return request(`${BASE_URL}/model/queryModelList`, METHOD.GET, payload)
}

export async function addModel(payload) {
  return request(`${BASE_URL}/model/addModel`, METHOD.POST, payload)
}

export async function update(payload) {
  return request(`${BASE_URL}/model`, METHOD.PUT, payload)
}

export async function deleteModel(payload) {
  return request(`${BASE_URL}/model/${payload}`, METHOD.DELETE)
}

export async function rename(payload) {
  return request(`${BASE_URL}/model/rename`, METHOD.PUT, payload)
}


