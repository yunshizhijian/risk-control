package com.wjz.engine.model;

import java.util.Map;

/**
 * @author wjz
 * @date 2021/9/12 19:57
 */
public interface AntiFraudEngine {
    /**
     * 特征/抽象指标的计算提取。
     * @param modelId
     * @param data
     * @return
     */
    Map<String, Object> executeAbstraction(Long modelId, Map<String, Map<String, ?>> data);

    /**
     * 激活器/策略集 模型的输出点，根据风险分数输出结果，拒绝（reject）或者人工审核(review),或者通过（pass）.
     * @param modelId
     * @param data
     * @return
     */
    Map<String, Object> executeActivation(Long modelId, Map<String, Map<String, ?>> data);
}
