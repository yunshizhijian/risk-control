package com.wjz.commons.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wjz.commons.entity.vo.RuleVo;
import com.wjz.commons.mapper.RuleVoMapper;
import org.springframework.stereotype.Service;

/**
 * @author wjz
 * @date 2021/9/8 21:00
 */
@Service
public class RuleApiService extends ServiceImpl<RuleVoMapper, RuleVo> {
}
