package com.wjz.commons.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wjz.commons.entity.vo.PreItemVo;
import com.wjz.commons.mapper.PreItemVoMapper;
import org.springframework.stereotype.Service;

/**
 * @author wjz
 * @date 2021/7/18 15:32
 */
@Service
public class PreItemApiService extends ServiceImpl<PreItemVoMapper, PreItemVo> {
}
