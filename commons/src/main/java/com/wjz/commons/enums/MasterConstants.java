package com.wjz.commons.enums;

/**
 * @author wjz
 * @date 2021/7/8 15:07
 */
public interface MasterConstants {

    /**
     * ModelVo状态常量 0-初始 1-可用 2-不可用
     */
    Integer INIT = 0;
    Integer ACTIVE = 1;
    Integer INACTIVE = 2;

    /**
     * AbstractionVo状态常量  1=新建
     */
    Integer NEW = 1;

    /**
     * ActivationVo状态常量  1=开  0=关
     */
    Integer CLOSE = 0;
    Integer OPEN = 1;
}
