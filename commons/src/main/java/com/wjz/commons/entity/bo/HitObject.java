package com.wjz.commons.entity.bo;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author wjz
 * @date 2021/9/12 19:43
 */
@Data
@Accessors(chain = true)
public class HitObject {

    private String key;

    private Double value;

    private String desc;
}
