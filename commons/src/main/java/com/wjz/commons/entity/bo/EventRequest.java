package com.wjz.commons.entity.bo;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author wjz
 * @date 2021/9/12 15:36
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "EventRequest", description = "事件信息")
public class EventRequest {

    @ApiModelProperty(value = "模型guid")
    private String guid;

    @ApiModelProperty(value = "请求流水号")
    private String reqId;

    @ApiModelProperty(value = "事件内容")
    private JSONObject jsonInfo;
}
