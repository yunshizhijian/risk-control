package com.wjz.commons.entity.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author wjz
 * @date 2021/7/7 14:28
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "ModelVo", description = "模型实体")
public class ModelVo implements Serializable {

    @TableId
    private Long objectId;

    private Long guid;

    private String modelName;

    private String label;

    private String entityName;

    private String entryName;

    private String referenceDate;

    private Integer status;

    private Date createTime;

    private Date updateTime;

    private Boolean fieldValidate;

    private String code;

    private Boolean template;
}
