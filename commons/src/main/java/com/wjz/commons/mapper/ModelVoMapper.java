package com.wjz.commons.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjz.commons.entity.vo.ModelVo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author wjz
 * @date 2021/7/7 15:25
 */
@Mapper
public interface ModelVoMapper extends BaseMapper<ModelVo> {
}
